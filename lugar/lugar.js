const axios = require("axios");

const getLugarLngLng = async (ciudad) => {
  const encodedUrl = encodeURI(ciudad);

  let key = "bfb82048dc044c06312a02579f76dbdb";
  const instance = axios.create({
    baseURL: `https://api.openweathermap.org/data/2.5/weather?q=${encodedUrl}&appid=${key}`,
  });

  const res = await instance.get();
  if (!res.data) {
    throw Error("Esta ciudad no existe");
  }
  const data = res.data;
  const direccion = data.name;
  const lat = data.coord.lat;
  const lng = data.coord.lon;
  return {
    direccion,
    lat,
    lng,
  };
};

module.exports = {
  getLugarLngLng,
};
