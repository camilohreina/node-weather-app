const axios = require("axios");

const getClima = async (lat, lng) => {
  const res = await axios.get(
    `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=bfb82048dc044c06312a02579f76dbdb&units=metric`
  );

  return res.data.main.temp;
};

module.exports = {
  getClima,
};
